import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Scanner;

public class Runner {
    private int n, k;
    private int currentPosition[] = {0, 0};
    private int currentTime = 0;
    private String currentWind;
    private Byte[] curreWindArray = {0, 0};
    private long visited = 1;
    private WindProvider windProvider;
    private Scanner scanner;

    public void start() {
        initFields();
        getNumbers();
        getData(new HashMap<Integer, LinkedHashSet<Integer>>());
        printNotVisited();
    }

    private void getNumbers() {
        n = scanner.nextInt();
        if (n < 3 || n > 100000)
            Utils.runError();
        k = scanner.nextInt();
        if (k < 2 || k > 1000000)
            Utils.runError();
    }

    private void printNotVisited() {

        System.out.println(((long) n) * ((long) n) - visited);
    }

    private void runRobot(HashMap<Integer, LinkedHashSet<Integer>> table, int time, String wind) {
        curreWindArray = windProvider.getMovement(currentWind);
        move(table, wind, time, curreWindArray);

    }

    private void lastMove(HashMap<Integer, LinkedHashSet<Integer>> table) {
        curreWindArray = windProvider.getMovement(currentWind);
        move(table, currentWind, currentTime + n, curreWindArray);
    }

    private int stepsToMove(Byte[] movement) {
        int min = n;
        if (movement[0] != 0) {
            if (movement[0] < 0) {
                min = currentPosition[0];
            } else {
                min = (n - 1) - currentPosition[0];
            }
        }
        if (movement[1] != 0) {
            if (movement[1] < 0) {
                min = (currentPosition[1] < min ? currentPosition[1] : min);
            } else {
                min = (((n - 1) - currentPosition[1]) < min ? ((n - 1) - currentPosition[1]) : min);
            }
        }
        return min;
    }

    private void move(HashMap<Integer, LinkedHashSet<Integer>> table, String wind, int time, Byte[] movement) {
        int min = stepsToMove(movement);
        min = (time - currentTime) < min ? time - currentTime : min;
        for (int i = 0; i < min; i++) {
            if (!(table.get(currentPosition[0] + movement[0]) != null && table.get(currentPosition[0] + movement[0]).contains(currentPosition[1] + movement[1]))) {
                currentPosition[0] = currentPosition[0] + movement[0];
                currentPosition[1] = currentPosition[1] + movement[1];
                if (table.get(currentPosition[0]) == null) {
                    LinkedHashSet<Integer> hashSet = new LinkedHashSet<>();
                    hashSet.add(currentPosition[1]);
                    table.put(currentPosition[0], hashSet);
                } else {
                    table.get(currentPosition[0]).add(currentPosition[1]);
                }
                visited++;
            } else
                break;
        }
        currentWind = wind;
        currentTime = time;
    }

    private void getData(HashMap<Integer, LinkedHashSet<Integer>> table) {
        int time;
        LinkedHashSet<Integer> hashSet = new LinkedHashSet<>();
        hashSet.add(0);
        table.put(0, hashSet);
        int nextInt = scanner.nextInt();
        currentWind = scanner.next();
        runRobot(table, nextInt, currentWind);
        for (int i = 1; i < k; i++) {
            time = scanner.nextInt();
            if (time <= nextInt || time >= 1000000)
                Utils.runError();
            runRobot(table, time, scanner.next());
            nextInt = time;
        }
        scanner.close();
        lastMove(table);
    }

    private void initFields() {
        windProvider = new WindProvider();
        scanner = new Scanner(System.in);
    }
}
