import java.util.HashMap;

public class WindProvider {
    HashMap<String, Byte[]> windMap;

    public WindProvider() {
        initMap();
    }

    private void initMap() {
        windMap = new HashMap<>();
        windMap.put("N", new Byte[]{-1, 0});
        windMap.put("S", new Byte[]{1, 0});
        windMap.put("W", new Byte[]{0, -1});
        windMap.put("E", new Byte[]{0, 1});
        windMap.put("NE", new Byte[]{-1, 1});
        windMap.put("NW", new Byte[]{-1, -1});
        windMap.put("SE", new Byte[]{1, 1});
        windMap.put("SW", new Byte[]{1, -1});
    }

    public Byte[] getMovement(String currentWind) {
        return windMap.get(currentWind);

    }
}
